<% if false %>
This file is part of Ballot at https://projects.kde.org/projects/websites/ballot-kde-org
Copyright 2011 Jeff Mitchell at mitchell@kde.org
Ballot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Ballot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Ballot. If not, see http://www.gnu.org/licenses/.
<% end %>

<div class="toc">
<ul>
<li><a href="#motivation">Motivation</a></li>
<li><a href="#why-is-it-so-ugly">Why is it so ugly?</a></li>
<li><a href="#features">Features</a></li>
<li><a href="#security-architecture">Security Architecture</a><ul>
<li><a href="#authentication-and-authorization">Authentication and Authorization</a></li>
<li><a href="#session-hijacking-prevention">Session Hijacking Prevention</a></li>
<li><a href="#user-private-keyschanging-votes">User Private Keys/Changing Votes</a></li>
<li><a href="#yubikeys">YubiKeys</a></li>
<li><a href="#scramble-votes">Scramble Votes</a></li>
</ul>
</li>
<li><a href="#attack-vectors">Attack Vectors</a><ul>
<li><a href="#hardcore-session-hijacking">Hardcore Session Hijacking</a></li>
<li><a href="#sysadmin-vectors">Sysadmin Vectors</a></li>
<li><a href="#vs-the-previous-kde-system-elektion">vs. the previous KDE system (Elektion)</a></li>
</ul>
</li>
</ul>
</div>
<h1 id="motivation">Motivation</h1>
<p>Ballot was created due to a confluence of two factors.</p>
<p>First, I wanted to replace the voting scripts KDE used (Elektion), for a few reasons:</p>
<ul>
<li>The voting scripts used email to send out tokens<ul>
<li>Email can get lost along the way (spam filters, for instance)</li>
<li>Email is quite easily intercepted (the system did not encrypt each email)</li>
<li>Access control to the system was based solely on the token sent out in the email, so anyone that gets their hands on it could vote as you</li>
</ul>
</li>
<li>You couldn't change your vote once you cast it<ul>
<li>People make mistakes and click the wrong button</li>
<li>If someone had intercepted the email and voted for you, you couldn't later change that vote to what you desired</li>
</ul>
</li>
</ul>
<p>So, I thought for a while about whether I could make a system that fixed at least some of these flaws and provided better security while doing so. Which leads me to the second factor: Identity. Now that KDE has a centralized authentication system, we can take advantage of that to ensure that the person voting is the <em>right</em> person. Ballot is the result.</p>
<h1 id="why-is-it-so-ugly">Why is it so ugly?</h1>
<p>Briefly, because I don't know a damn about web UI programming other than very basic HTML. On the flip side, this means that I didn't feel tempted to add fancy features that might look nicer or be super JavaScripty-cool but affect the security posture.</p>
<p>That said, if anyone wants to lend their CSS skills to make it look nicer without introducing complexity, I'm open for it.</p>
<p>P.S.: This goes for the code too. I know that if you're a Ruby god the code will probably make you scream bloody murder, but it's relatively modular due to Sinatra's architecture and thus not terribly difficult to understand (if you understand the whole, you can look at the parts and verify that they are behaving correctly).</p>
<h1 id="features">Features</h1>
<ul>
<li>Single Sign In using LDAP (for instance, to Identity)</li>
<li>Multiple votes can be run at any time, each with their own configuration and set of authorized users</li>
<li>Ability to see open votes accessible to you, as well as their expiration time</li>
<li>Ability to change your vote, if you remember your secret key</li>
<li>Supports YubiKeys for second-factor authentication</li>
<li>Scramble votes for the super-paranoid</li>
</ul>
<h1 id="security-architecture">Security Architecture</h1>
<p>Before getting into the below, more specific sections, a word: Ballot must be run over HTTPS. All pages, all the time. You simply cannot have a secure voting site that is waiting for any eavesdropper that's around to pick out your traffic.</p>
<p>There are some factors built into Ballot (covered below) that can help ensure that your session isn't highjacked, if for some reason you have only the login page over HTTPS, but if the black helicopters are watching your username and password and secret keys fly by in cleartext, that doesn't help much. Not to mention that if vote pages are in cleartext, people will see your vote selections anyways (except in the case of scramble votes, below). Run over HTTPS.</p>
<h2 id="authentication-and-authorization">Authentication and Authorization</h2>
<p>The only actions in Ballot that do not go through some sort of authentication check are:</p>
<ul>
<li>Accessing the index page</li>
<li>Accessing this About page</li>
</ul>
<p>That's it. Any other page checks your credentials and responds appropriately, usually by redirecting you back to the login page if the credential check fails. This includes the logout page, which ensures that the person attempting to logout is actually the person they claim that they are (so that an attacker can't cause you problems by logging you out repeatedly). Therefore, other than accessing those static pages, there is no action you can perform in Ballot that does not require a credential check.</p>
<p>Once you log in, which verifies your given information against Identity, your password is forgotten. Your rights to use the system are then stored in a credential cookie that is freshly set on every single access of an authenticated page (see Session Highjacking Prevention, below).</p>
<p>Votes are loaded from .vote files, which are JSON-syntax files containing vote data. Votes are never loaded based on vote names given by the user, to avoid directory traversal or other similar attacks. When a user requests a vote, an exact string match against the system's known votes is performed. If the vote exists, an authorization check is performed to ensure that the user belongs to one of the LDAP groups/users to which the vote explicitly allows access. This authorization check is performed every single time you perform a GET or POST on a vote.</p>
<h2 id="session-hijacking-prevention">Session Hijacking Prevention</h2>
<p>In order to prevent session hijacking, a rotating cookie (a "One Time Cookie", based off the similar concept of a "One Time Password") is used. This cookie has a few parts:</p>
<ul>
<li>A randomly-sized hex prefix, somewhere between 0 and 1000 hex characters long</li>
<li>A SHA256 hash made up of both a few static factors (the user's login name and IP address, for instance) as well as a random float value converted into a string</li>
<li>A randomly-sized hex suffix, somewhere between 0 and 1000 hex characters long</li>
</ul>
<p>The values of the hex prefix, hex suffix, and the random float in the hash are stored server-side and associated with that user. When an action is performed on the server, the cookie's presented hash is matched against a reconstructed hash from the server's stored random values for that user. If it does not match, the user is not authorized and the action is prevented. If it matches, a new random value is generated and the user's cookie re-set with a new prefix, SHA256 (with a new random float), and new suffix.</p>
<p>A few factors make it very difficult for an attacker to make use of this cookie, especially under HTTPS (see the Attack Vectors subsection Hardcore Session Hijacking for more information). An attacker may know that there is a SHA256 hash embedded in the broader string, but won't know where it is, since both the size of the overall string and the SHA256's position inside the string is generated anew every access. Additionally, since the IP address of the connection is embedded in the SHA256 hash, and the SHA256 hash is reconstructed on the server during the check with the current connection's IP address, an attacker wishing to make use of a discovered cookie would need to either be in behind the same NAT area or have some other way to spoof the original connection's IP address.</p>
<p>Finally, since each cookie value is good for only one access of the system, it does mean that no two users will be able to use the system under the same account at the same time; this means that a user in a situation where cookie stealing is possible that suddenly finds themselves unauthenticated can know that their session may have been hijacked.</p>
<h2 id="user-private-keyschanging-votes">User Private Keys/Changing Votes</h2>
<p>In order to allow users to change their votes with a minimal of attack vector risk, the user's record of voting is obfuscated. The information about votes is split up into two directories. One records which users have voted. When a user votes, their username is hashed with a vote-specific key (so that the same user won't have the same identifier in two different votes) and a file is created indicating that that user has voted (this directory can be made public or not depending on the desires of those running the system).</p>
<p>The other directory contains the votes themselves. The file values contain the selected voting options; the file names are a hash of the username and their vote-specific private key.</p>
<p>How this works is that when a user attempts to vote, their username/vote key hash is created and a check is run to see if a file with this hash value exists in the user directory. If the hash doesn't exist, the vote is recorded and all is well. However, if the hash does exist, then the system knows that it must match the user's given private key, hashed with their username, against the file names in the vote directory. If such a hash is found, the user gave the correct private key and the changed vote is recorded. If it isn't found, the user's supplied private key is incorrect and their attempt to change the vote is denied.</p>
<h2 id="yubikeys">YubiKeys</h2>
<p>The system is capable of supporting, on a per-user basis, YubiKeys for second-factor authentication. If turned on globally, users will see a login field asking for a YubiKey one-time password (OTP). If their account has a YubiKey public identifier associated with it, this field becomes required. This OTP is then checked against Yubico's servers (currently, checking aginst other servers is not supported). If the OTP is not valid, the user is not allowed to log in.</p>
<h2 id="scramble-votes">Scramble Votes</h2>
<p>For the ultimate in paranoia, votes can be scrambled. Scramble votes replace the normal vote options with random integer values between 1 and 10,000,000, randomizes the order in which the values are shown on the web page, and changes the value and order on every single page view. In scramble votes, the user is shown a button that can be pressed to send an SMS to the phone number they have recorded in Identity (right now this is disabled, as my testing was only with a personal Google Voice account, which worked). Pressing the button causes a refresh of the page (thus re-scrambling the values) and sends the decoded values for that one page view to the user's phone. The user can then select their desired vote value.</p>
<p>Scramble votes are really only useful in cases where you can't use HTTPS or cases where HTTPS is being intercepted (see the Attack Vectors subsection Hardcore Session Hijacking for more information).</p>
<h1 id="attack-vectors">Attack Vectors</h1>
<p>I do not believe that the system has a lot of attack vectors. There is no SQL (SQL Injection); no references to external sites (XSS); no JavaScript is used; no user inputs are trusted (all user inputs are either string-compared to previously known values to prevent e.g. filesystem traversal attacks, or are directly hashed). However, any that I know of, and a discussion of them, are listed here.</p>
<h2 id="hardcore-session-hijacking">Hardcore Session Hijacking</h2>
<p>In nearly all cases, the cookie randomization feature is enough to prevent session hijacking, in combination with HTTPS. However, you could have a problem if you either cannot use HTTPS, or if your employer/government/etc has put into place a nasty system (they exist) whereby you are required to have trusted the certificate of a root certification authority controlled by your employer/goverment/etc and also required to go through them for HTTPS, giving them the perfect avenue for a man-in-the-middle attack. In such a case, they can actually intercept your HTTPS calls, creating valid certificates on-the-fly to the server's endpoint so that your browser thinks everything is fine but meanwhile they're sniffing your decoded traffic.</p>
<p>Note that the IP address of the attacker will still have to match the user's originating IP address, so this provides some level of security. Additionally, SSL attacks (for instance, BEAST) currently require a great deal of time to decrypt a user's session cookie (on the order of many seconds per byte). In a normal user session (especially in a scenario where the user logs out properly), by the time that such an attack could be carried out, the user's cookie will have changed multiple times, and the decrypted cookie will no longer be valid.</p>
<p>If all else fails, you can either deal with it (keeping in mind to be wary of your authenticated access to the system suddenly disappearing) or you can use scramble votes. Assuming that you trust your telecom provider. :-)</p>
<h2 id="sysadmin-vectors">Sysadmin Vectors</h2>
<p>If you don't trust your sysadmins, you're lost. That is almost universally true. The clever sysadmin can -- especially with open-source software such as this, and especially with root access to a filesystem -- nearly always find a way to defeat all sorts of security measures. There are various things a sysadmin could do to figure out who is voting what:</p>
<ul>
<li>Write a script (or just watch on the command line) to correlate the records of which users have voted with the votes that were recorded at the same time, then decode the values of the user hashes</li>
<li>Rewrite Ballot to spit out the values as they come in, along with the usernames</li>
<li>Etc.</li>
</ul>
<p>The lesson is: it wouldn't be hard. (It isn't hard on the current system either; see below.) So don't ever use this software -- or any centrally-hosted voting software -- if you don't trust the person running the server that is hosting it.</p>
<h2 id="vs-the-previous-kde-system-elektion">vs. the previous KDE system (Elektion)</h2>
<p>The vectors are actually quite similar, and essentially boil down to the same point made above: you have to trust your sysadmin.</p>
<ul>
<li>A sysadmin could watch and save the mail as it was being sent out from Elektion, to know what address got what token; a sysadmin could watch user hashes and votes as they're being recorded in Ballot. However, in this instance, Ballot is an improvement; while the sysadmin can decode the user hashes and watch the votes, at least third parties along the SMTP path couldn't intercept the mail as well.</li>
<li>Elektion scripts could be modified to record what email was sent what token, and what token voted what. Ballot can be modified to perform similar recording.</li>
<li>With Elektion, you could know which tokens voted and which didn't. With Ballot, you can resolve that to usernames (even if you haven't done one of the above two things) with some scripting. Once the vote is done, however, noting says that you need to keep the list of usernames that voted around; so if you wish you can remove that list and keep only the results around, and those <em>cannot</em> be correlated to specific users without more information (such as by using one of the two methods above).</li>
</ul>